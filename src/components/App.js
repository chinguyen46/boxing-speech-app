import React from 'react';
import Button from './Button.js';


class App extends React.Component{

    state = {turnOn: false, number: null};
    intervalID = 0; 

    turnAppOn = () =>{
        this.setState({turnOn: true}, () =>{
            console.log('on');
            this.intervalID = setInterval(this.generateRandomNum, 1500);
        });
    };

    turnAppOff = () =>{
        this.setState({turnOn: false, number: null}, () =>{
            console.log('off');
            clearInterval(this.intervalID);
        });
    };

    generateRandomNum = () =>{
        let num = Math.ceil(Math.random() * 5);
        let utterance = new SpeechSynthesisUtterance(num);
        utterance.rate = 1.5;
        utterance.volume = 3;
        this.setState({number: num}, () =>{
            window.speechSynthesis.speak(utterance);
            console.log(utterance);
        });  
    };


    render(){
        return(
            <div className="button-position">
                 <h1 className="">{this.state.number}</h1>
                <Button turnAppOn={this.turnAppOn} turnAppOff={this.turnAppOff} value={this.state.turnOn}/>
            </div>
            
        )
    }
};

export default App; 
