import React from 'react';

const Button = props => {

    return (
        <div className="">
            {!props.value ? <button className="btn btn-success btn-md" onClick={props.turnAppOn}>Click to turn on</button> : <button className="btn btn-danger btn-md" onClick={props.turnAppOff}>Click to turn off</button>}
        </div>
    )
}

export default Button; 
