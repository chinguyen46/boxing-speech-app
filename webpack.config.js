const path = require("path");


module.exports = {
	entry: './src/index.js',
	output:{
		path: path.join(__dirname, '/public/dist/'),
		filename: 'bundle.js'
	},
	watch: true,
	devtool: "source-map",
	module:{
		rules: 
			[{
			test: /\.(js|jsx)$/,
			exclude: /node_modules/,
			use: [ 'babel-loader']	 
			},
			{
			test: /\.css$/i,
			use: [ 'style-loader', 'css-loader']	
			}]
		}
};
